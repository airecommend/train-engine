source('./funs.R')

db_path <- './data/air.sqlite'
model_path <- './data/model.hdf5'
items_path <- './data/train_items.json'


if (!findDb(db = db_path)) { 
  stop('Can not locate the database to train from.')} else
    require(jsonlite)
    items_to_train <- findPredictors(db = db_path)
    
    if (length(items_to_train) == 0) {
      stop('Can not train on empty database')} else 
        if (length(items_to_train) < 10) {'Too few items to train from'} else 
          { 
    write_json(items_to_train,'./data/train_items.json')
    doTrain(db_path, model_path)
}
