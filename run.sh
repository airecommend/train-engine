#!/usr/bin/env bash

#install packages
Rscript -e 'if(!require("keras")) {install.packages("keras", dependencies = T, repos = "https://cran.rstudio.com/")}'
Rscript -e 'if(!require("RSQLite")) {install.packages("RSQLite", dependencies = T, repos = "https://cran.rstudio.com/")}'
Rscript -e 'if(!require("DBI")) {install.packages("DBI", dependencies = T, repos = "https://cran.rstudio.com/")}'

#run app
Rscript -e 'app <- plumber::plumb("./api.R"); app$run(host="0.0.0.0", port=8000, swagger=T)'
