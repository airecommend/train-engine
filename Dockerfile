FROM rstudio/plumber

RUN R -e ".libPaths(c( './Rlib', .libPaths()))"
RUN R -e "install.packages(c('jsonlite','abc','RSQLite','DBI','digest', 'keras', 'tidyr', 'data.table'))"
RUN R -e "reticulate::install_miniconda()"
RUN R -e "library(keras); install_keras()"

ADD funs.R /funs.R
ADD train.R /train.R
ADD run.sh /run.sh
ADD api.R /api.R

RUN apt-get -y install cron && \
    apt-get -y install docker.io

ADD crontab /etc/cron.d/crontab
ADD run_model-engine.sh /run_model-enginesh
ADD run_train-engine.sh /run_train-enginesh

RUN chmod 0700 /etc/cron.d/crontab \
    && chmod 0700 /run_train-enginesh \
    && chmod 0700 /run_model-enginesh

ENTRYPOINT ["/bin/bash","./run.sh"]
