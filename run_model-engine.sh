#!/bin/sh

#on the host: docker run --rm -v air:/data -v /var/run/docker.sock:/var/run/docker.sock -p 8001:8001 db

echo "$(date) model restart initiated" >> /data/schedule_log

if ! docker ps | grep -q train; then
    echo "$(date) train is not running, proceeding" >> /data/schedule_log

    if docker ps | grep -q air; then 
        echo "$(date) model is running, restarting" >> /data/schedule_log
        current=$(docker ps | grep air | awk {'print $1'})
        docker container restart $current
    else
        echo "$(date) model is NOT running, starting" >> /data/schedule_log
        docker run --rm -d --volumes-from $(basename "$(head /proc/self/cgroup)") air
    fi
    echo "$(date) model enpoint (re)started" >> /data/schedule_log
else 
    echo "$(date) train is running, sleeping" >> /data/schedule_log
fi
